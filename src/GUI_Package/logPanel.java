package GUI_Package;

import Functional_Package.barangModel;
import Functional_Package.customTableModel;
import Functional_Package.getConnection;
import Functional_Package.logModel;
import java.awt.Font;
import java.awt.Image;
import static java.lang.reflect.Array.set;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import static javax.management.remote.JMXConnectorFactory.connect;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class logPanel extends javax.swing.JPanel {

    private getConnection connect;
    private Connection con;
    private Statement state;
    private ResultSet set;
    private ArrayList<logModel> data;
    private logModel model;

    public logPanel() throws SQLException {
        initComponents();

        fetch();
        fill();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        groupradio = new javax.swing.ButtonGroup();
        tableScroller = new javax.swing.JScrollPane();
        inventoryTable = new javax.swing.JTable();

        setMinimumSize(new java.awt.Dimension(660, 530));
        setPreferredSize(new java.awt.Dimension(660, 530));

        inventoryTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Barang", "Jenis", "Jumlah", "Tanggal"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableScroller.setViewportView(inventoryTable);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tableScroller, javax.swing.GroupLayout.DEFAULT_SIZE, 640, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tableScroller, javax.swing.GroupLayout.DEFAULT_SIZE, 508, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void fetch() throws SQLException {

        // ambil data dari database
        // inisiasi kelas get connection
        connect = new getConnection();

        // mulai koneksi
        con = connect.connect();

        // persiapan query
        state = con.createStatement();

        // eksekusi query
        set = state.executeQuery("SELECT log.id, barang.nama, log.jenis, log.jumlah, log.tanggal FROM log INNER JOIN barang ON log.sku = barang.sku");

        // simpan hasil query ke model log
        data = new ArrayList<logModel>();

        while (set.next()) {

            model = new logModel(set.getInt(1), set.getString(2), set.getString(3), set.getInt(4), set.getString(5));

            data.add(model);
        }
    }

    private void fill() {

        // isi tabel log
        // data dalam arraylist class barangModel dipindahkan ke tabel
        DefaultTableModel tableModel = (DefaultTableModel) inventoryTable.getModel();

        for (int i = 0; i < data.size(); i++) {

            tableModel.addRow(new Object[]{data.get(i).getID(), data.get(i).getNama(),
                data.get(i).getJenis(), data.get(i).getJumlah(), data.get(i).getTanggal()
            });
        }

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);

        inventoryTable.setRowHeight(24);
        inventoryTable.setFont(new Font("Tahoma", Font.PLAIN, 14));

        // preferensi content alignment pada tabel
        inventoryTable.getColumnModel().getColumn(0).setHeaderRenderer(centerRenderer);
        inventoryTable.getColumnModel().getColumn(1).setHeaderRenderer(centerRenderer);
        inventoryTable.getColumnModel().getColumn(2).setHeaderRenderer(centerRenderer);
        inventoryTable.getColumnModel().getColumn(3).setHeaderRenderer(centerRenderer);
        inventoryTable.getColumnModel().getColumn(4).setHeaderRenderer(centerRenderer);
        inventoryTable.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        inventoryTable.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        inventoryTable.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        inventoryTable.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        inventoryTable.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);

        inventoryTable.setModel(tableModel);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup groupradio;
    private javax.swing.JTable inventoryTable;
    private javax.swing.JScrollPane tableScroller;
    // End of variables declaration//GEN-END:variables
}
