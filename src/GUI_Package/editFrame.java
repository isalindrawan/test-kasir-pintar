package GUI_Package;

import Functional_Package.barangModel;
import Functional_Package.fileToByte;
import Functional_Package.getEdit;
import Functional_Package.getImage;
import Functional_Package.getLogID;
import Functional_Package.hapusData;
import Functional_Package.hapusLog;
import Functional_Package.loggingData;
import Functional_Package.updateData;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.showMessageDialog;

public class editFrame extends javax.swing.JFrame {

    public editFrame(int sku) throws SQLException {

        initComponents();

        fetch(sku);
        fill();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        editPanel = new javax.swing.JPanel();
        skuLabel = new javax.swing.JLabel();
        namaLabel = new javax.swing.JLabel();
        stokLabel = new javax.swing.JLabel();
        gambarLabel = new javax.swing.JLabel();
        skuField = new javax.swing.JTextField();
        namaField = new javax.swing.JTextField();
        imageContainer = new javax.swing.JLabel();
        simpanButton = new javax.swing.JButton();
        hapusButton = new javax.swing.JButton();
        stokSpinner = new javax.swing.JSpinner();
        bukaButton = new javax.swing.JButton();
        batalButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Edit Data");
        setMinimumSize(new java.awt.Dimension(290, 533));
        setName("editFrame"); // NOI18N
        setResizable(false);

        skuLabel.setText("SKU");

        namaLabel.setText("Nama");

        stokLabel.setText("Stok");

        gambarLabel.setText("Gambar");

        skuField.setEditable(false);

        imageContainer.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        imageContainer.setText("No Image");
        imageContainer.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        imageContainer.setMaximumSize(new java.awt.Dimension(250, 250));
        imageContainer.setMinimumSize(new java.awt.Dimension(250, 250));
        imageContainer.setPreferredSize(new java.awt.Dimension(250, 250));

        simpanButton.setText("Simpan");
        simpanButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simpanButtonActionPerformed(evt);
            }
        });

        hapusButton.setText("Hapus");
        hapusButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hapusButtonActionPerformed(evt);
            }
        });

        bukaButton.setText("Buka");
        bukaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bukaButtonActionPerformed(evt);
            }
        });

        batalButton.setText("Batal");
        batalButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                batalButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout editPanelLayout = new javax.swing.GroupLayout(editPanel);
        editPanel.setLayout(editPanelLayout);
        editPanelLayout.setHorizontalGroup(
            editPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, editPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(editPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(batalButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(hapusButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(simpanButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, editPanelLayout.createSequentialGroup()
                        .addComponent(skuLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(skuField))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, editPanelLayout.createSequentialGroup()
                        .addGroup(editPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(stokLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(namaLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(editPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(namaField)
                            .addComponent(stokSpinner)))
                    .addGroup(editPanelLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(editPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(editPanelLayout.createSequentialGroup()
                                .addComponent(gambarLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(bukaButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(imageContainer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        editPanelLayout.setVerticalGroup(
            editPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(editPanelLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(editPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(skuLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(skuField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(editPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(namaLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(namaField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(editPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(stokLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stokSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(editPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(gambarLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bukaButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(imageContainer, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(simpanButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(hapusButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(batalButton)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(editPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(editPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void simpanButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simpanButtonActionPerformed

        // melakukan pengecekan apabila ada field yang kosong
        if (checkField()) {

            // simpan inputan pada variable sementara
            int sku = Integer.valueOf(skuField.getText());
            String nama = namaField.getText();
            int stok = Integer.valueOf(stokSpinner.getValue().toString());

            // hitung selisih stok
            int diff = stok - data.stok();

            // cek log jenis
            String jenis = checkOperation();

            try {

                // generate log id
                getLogID id = new getLogID();
                int logID = id.getLogID();

                // inisiasi dan eksekusi class saveData untuk menyimpan data ke db
                updateData update = new updateData(sku, nama, stok, image);
                loggingData log = new loggingData(logID, jenis, diff, sku);

            } catch (FileNotFoundException ex) {
                Logger.getLogger(inventoryPanel.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(inventoryPanel.class.getName()).log(Level.SEVERE, null, ex);
            }

            dispose();

        } else {

            // tampilkan pesan error ketika terjadi kesalahan
            showMessageDialog(null, "mohon isi field nama dan stok lebih besar atau sama dengan 0");
        }
    }//GEN-LAST:event_simpanButtonActionPerformed

    private void hapusButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hapusButtonActionPerformed

        // tampilkan peringatan
        int dialogResult = JOptionPane.showConfirmDialog(null, "Perubahan akan anda buat akan dibatalkan, apakah anda yakin ?");

        if (dialogResult == JOptionPane.YES_OPTION) {

            try {

                // proses penghapusan dengan memanggil kelas hapuslog dan hapusdata
                hapusLog hapusLog = new hapusLog(data.sku());
                hapusData hapus = new hapusData(data.sku());

            } catch (FileNotFoundException ex) {
                Logger.getLogger(editFrame.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(editFrame.class.getName()).log(Level.SEVERE, null, ex);
            }

            dispose();

        }
    }//GEN-LAST:event_hapusButtonActionPerformed

    private void bukaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bukaButtonActionPerformed

        // inisiasi getImage class
        getImage open = new getImage();

        // simpan file gambar yang diambil dari class getImage
        file = open.getFile();

        try {

            // konversi file ke byte
            fileToByte convert = new fileToByte(file);
            image = convert.getByte();

        } catch (IOException ex) {
            Logger.getLogger(editFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        // set preview untuk gambar pada panel
        BufferedImage im = open.getSourceImage();
        imageContainer.setText("");
        imageContainer.setIcon(new ImageIcon(im.getScaledInstance(250, 250, Image.SCALE_SMOOTH)));
    }//GEN-LAST:event_bukaButtonActionPerformed

    private void batalButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_batalButtonActionPerformed

        // tampilkan peringatan
        int dialogResult = JOptionPane.showConfirmDialog(null, "Perubahan akan anda buat akan dibatalkan, apakah anda yakin ?");

        if (dialogResult == JOptionPane.YES_OPTION) {

            dispose();

        }
    }//GEN-LAST:event_batalButtonActionPerformed

    private void fetch(int sku) throws SQLException {

        // ambil data
        getEdit get = new getEdit(sku);
        data = get.getData();
    }

    private void fill() {

        // isi data pada fileds
        image = data.image();

        skuField.setText(String.valueOf(data.sku()));
        namaField.setText(data.nama());
        stokSpinner.setValue(data.stok());

        if (data.image() != null) {

            ImageIcon image = new ImageIcon(
                    new ImageIcon(data.image())
                            .getImage().getScaledInstance(250, 250, Image.SCALE_SMOOTH)
            );

            imageContainer.setIcon(image);

        }
    }

    private boolean checkField() {

        boolean state = false;

        // melakukan pengecekan terhadap inputan, apabila tidak kosong, ubah state
        if (!namaField.getText().isEmpty()) {

            if (Integer.parseInt(stokSpinner.getValue().toString()) >= 0) {

                state = true;
            }
        }

        return state;
    }

    private String checkOperation() {

        // menentukan jenis perubahan pada data log
        String operation = "Tambah";

        if (data.stok() > Integer.valueOf(stokSpinner.getValue().toString())) {

            operation = "Kurang";

        }

        return operation;
    }

    private byte[] image;
    private File file;
    private barangModel data;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton batalButton;
    private javax.swing.JButton bukaButton;
    private javax.swing.JPanel editPanel;
    private javax.swing.JLabel gambarLabel;
    private javax.swing.JButton hapusButton;
    private javax.swing.JLabel imageContainer;
    private javax.swing.JTextField namaField;
    private javax.swing.JLabel namaLabel;
    private javax.swing.JButton simpanButton;
    private javax.swing.JTextField skuField;
    private javax.swing.JLabel skuLabel;
    private javax.swing.JLabel stokLabel;
    private javax.swing.JSpinner stokSpinner;
    // End of variables declaration//GEN-END:variables
}
