package GUI_Package;

import Functional_Package.*;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;

public class inventoryPanel extends javax.swing.JPanel {

    public inventoryPanel() throws SQLException {
        initComponents();

        // memanggil method fetch & fill
        fetch();
        fill();

        // memanggil method getSKU
        generateSKU();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        groupradio = new javax.swing.ButtonGroup();
        inputPanel = new javax.swing.JPanel();
        skuLabel = new javax.swing.JLabel();
        namaLabel = new javax.swing.JLabel();
        stokLabel = new javax.swing.JLabel();
        gambarLabel = new javax.swing.JLabel();
        skuField = new javax.swing.JTextField();
        namaField = new javax.swing.JTextField();
        imageContainer = new javax.swing.JLabel();
        simpanButton = new javax.swing.JButton();
        batalButton = new javax.swing.JButton();
        stokSpinner = new javax.swing.JSpinner();
        bukaButton = new javax.swing.JButton();
        tableScroller = new javax.swing.JScrollPane();
        inventoryTable = new javax.swing.JTable();

        setMinimumSize(new java.awt.Dimension(660, 530));
        setPreferredSize(new java.awt.Dimension(660, 530));

        inputPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Input Data", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP));

        skuLabel.setText("SKU");

        namaLabel.setText("Nama");

        stokLabel.setText("Stok");

        gambarLabel.setText("Gambar");

        skuField.setEditable(false);

        imageContainer.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        imageContainer.setText("No Image");
        imageContainer.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        imageContainer.setMaximumSize(new java.awt.Dimension(250, 250));
        imageContainer.setMinimumSize(new java.awt.Dimension(250, 250));
        imageContainer.setPreferredSize(new java.awt.Dimension(250, 250));

        simpanButton.setText("Simpan");
        simpanButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simpanButtonActionPerformed(evt);
            }
        });

        batalButton.setText("Batal");
        batalButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                batalButtonActionPerformed(evt);
            }
        });

        bukaButton.setText("Buka");
        bukaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bukaButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout inputPanelLayout = new javax.swing.GroupLayout(inputPanel);
        inputPanel.setLayout(inputPanelLayout);
        inputPanelLayout.setHorizontalGroup(
            inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inputPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(imageContainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(batalButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(inputPanelLayout.createSequentialGroup()
                        .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(stokLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(namaLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(stokSpinner)
                            .addComponent(namaField)))
                    .addGroup(inputPanelLayout.createSequentialGroup()
                        .addComponent(skuLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(skuField))
                    .addGroup(inputPanelLayout.createSequentialGroup()
                        .addComponent(gambarLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(bukaButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, inputPanelLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(simpanButton, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        inputPanelLayout.setVerticalGroup(
            inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(inputPanelLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(skuLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(skuField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(namaLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(namaField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(stokLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stokSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(inputPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(gambarLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bukaButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(imageContainer, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(simpanButton)
                .addGap(11, 11, 11)
                .addComponent(batalButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tableScroller.setMinimumSize(new java.awt.Dimension(345, 508));
        tableScroller.setPreferredSize(new java.awt.Dimension(345, 508));

        inventoryTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        inventoryTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                inventoryTableMouseClicked(evt);
            }
        });
        tableScroller.setViewportView(inventoryTable);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(inputPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tableScroller, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tableScroller, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(inputPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void bukaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bukaButtonActionPerformed

        // inisiasi getImage class
        getImage open = new getImage();

        // simpan file gambar yang diambil dari class getImage
        file = open.getFile();
        image = open.getSourceImage();

        // set preview untuk gambar pada panel
        imageContainer.setText("");
        imageContainer.setIcon(new ImageIcon(image.getScaledInstance(250, 250, Image.SCALE_SMOOTH)));

    }//GEN-LAST:event_bukaButtonActionPerformed

    private void simpanButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simpanButtonActionPerformed

        // melakukan pengecekan apabila ada field yang kosong
        if (checkField()) {

            // simpan inputan pada variable sementara
            int sku = Integer.valueOf(skuField.getText());
            String nama = namaField.getText();
            int stok = Integer.valueOf(stokSpinner.getValue().toString());

            try {

                // generate log id
                getLogID id = new getLogID();
                int logID = id.getLogID();

                // inisiasi dan eksekusi class saveData untuk menyimpan data ke db
                saveData save = new saveData(sku, nama, stok, file);
                loggingData log = new loggingData(logID, "Tambah", stok, sku);

            } catch (FileNotFoundException ex) {
                Logger.getLogger(inventoryPanel.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(inventoryPanel.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {

            // tampilkan pesan error ketika terjadi kesalahan
            showMessageDialog(null, "mohon isi field nama dan stok lebih besar atau sama dengan 0");
        }

        try {

            // muat ulang GUI setelah input data selesai
            reload();

        } catch (SQLException ex) {
            Logger.getLogger(inventoryPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_simpanButtonActionPerformed

    private void batalButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_batalButtonActionPerformed

        try {

            reload();

        } catch (SQLException ex) {
            Logger.getLogger(inventoryPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_batalButtonActionPerformed

    private void inventoryTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_inventoryTableMouseClicked

        // mengambil data pada baris tertentu untuk di tampilkan pada halaman edit / preview
        try {

            // ambil index kolom dan baris untuk di tampilkan
            int row = inventoryTable.getSelectedRow();
            int sku = (int) inventoryTable.getValueAt(row, 0);
            editFrame edit;

            // tampilkan pada halaman edit
            edit = new editFrame(sku);
            edit.setVisible(true);

            // cek apakah halaman edit tertutup
            edit.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent we) {

                    // muat ulang halaman inventory apabila edit window tertutup
                    try {

                        reload();
                        fetch();
                        fill();

                    } catch (SQLException ex) {
                        Logger.getLogger(inventoryPanel.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });

        } catch (SQLException ex) {
            Logger.getLogger(inventoryPanel.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_inventoryTableMouseClicked

    private void fetch() throws SQLException {

        // mengambil semua data dati tabel barang
        // inisiasi kelas get connection
        connect = new getConnection();

        // mulai koneksi
        con = connect.connect();

        // persiapan query
        state = con.createStatement();

        //eksekusi query
        set = state.executeQuery("SELECT * FROM barang ");

        // simpan hasil query ke model barang
        data = new ArrayList<barangModel>();

        while (set.next()) {

            model = new barangModel(set.getInt(1), set.getString(2), set.getInt(3), set.getBytes(4));

            data.add(model);
        }
    }

    private void fill() {

        // isi tabel inventory
        // data dalam arraylist class barangModel dipindahkan ke tabel
        String[] column = {"SKU", "Nama", "Stok", "Gambar"};
        Object[][] rows = new Object[data.size()][4];

        for (int i = 0; i < data.size(); i++) {

            rows[i][0] = data.get(i).sku();
            rows[i][1] = data.get(i).nama();
            rows[i][2] = data.get(i).stok();

            if (data.get(i).image() != null) {

                ImageIcon image = new ImageIcon(
                        new ImageIcon(data.get(i).image())
                                .getImage().getScaledInstance(64, 64, Image.SCALE_SMOOTH)
                );

                rows[i][3] = image;

            } else {

                rows[i][3] = null;
            }
        }

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);

        // inisiasi custom tabel model
        customTableModel model = new customTableModel(rows, column);
        inventoryTable.setModel(model);

        // preferensi tampilan tabel
        inventoryTable.setRowHeight(64);
        inventoryTable.setFont(new Font("Tahoma", Font.PLAIN, 14));

        // preferensi content alignment pada tabel
        inventoryTable.getColumnModel().getColumn(0).setHeaderRenderer(centerRenderer);
        inventoryTable.getColumnModel().getColumn(1).setHeaderRenderer(centerRenderer);
        inventoryTable.getColumnModel().getColumn(2).setHeaderRenderer(centerRenderer);
        inventoryTable.getColumnModel().getColumn(3).setHeaderRenderer(centerRenderer);
        inventoryTable.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        inventoryTable.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        inventoryTable.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);

        inventoryTable.getColumnModel().getColumn(3).setPreferredWidth(64);
    }

    private void generateSKU() throws SQLException {

        // method mengambil sku terakhir
        getSKU sku = new getSKU();

        // tampilkan di field sku
        skuField.setText(String.valueOf(sku.getSKU()));
    }

    private boolean checkField() {

        // method untuk pengecekan filed inputan
        boolean state = false;

        // melakukan pengecekan terhadap inputan, apabila tidak kosong, ubah state
        if (!namaField.getText().isEmpty()) {

            if (Integer.parseInt(stokSpinner.getValue().toString()) >= 0) {

                state = true;
            }
        }

        return state;
    }

    private void reload() throws SQLException {
        // reset / refresh class inventoryPanel setelah proses input
        inventoryTable.removeAll();

        file = null;
        image = null;
        set = null;
        con = null;
        data = null;
        model = null;
        connect = null;

        imageContainer.setIcon(null);
        namaField.setText("");
        stokSpinner.setValue(0);

        fetch();
        fill();
        generateSKU();
        imageContainer.setText("No Image");

    }

    private File file = null;
    private BufferedImage image = null;

    private Statement state;
    private ResultSet set;
    private Connection con;
    private ArrayList<barangModel> data;
    private barangModel model;
    private getConnection connect;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton batalButton;
    private javax.swing.JButton bukaButton;
    private javax.swing.JLabel gambarLabel;
    private javax.swing.ButtonGroup groupradio;
    private javax.swing.JLabel imageContainer;
    private javax.swing.JPanel inputPanel;
    private javax.swing.JTable inventoryTable;
    private javax.swing.JTextField namaField;
    private javax.swing.JLabel namaLabel;
    private javax.swing.JButton simpanButton;
    private javax.swing.JTextField skuField;
    private javax.swing.JLabel skuLabel;
    private javax.swing.JLabel stokLabel;
    private javax.swing.JSpinner stokSpinner;
    private javax.swing.JScrollPane tableScroller;
    // End of variables declaration//GEN-END:variables
}
