package Functional_Package;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

// class untuk mengambil nomor sku pada db
public class getSKU {

    private Statement state;
    private ResultSet set;
    private Connection con;
    private getConnection connect;
    private int sku;

    public int getSKU() throws SQLException {

        // inisiasi kelas get connection
        connect = new getConnection();

        // mulai koneksi
        con = connect.connect();

        // persiapan query
        state = con.createStatement();

        // eksekusi query
        set = state.executeQuery("SELECT `sku` FROM barang ORDER BY `sku` DESC LIMIT 1");

        // cek apakah hasil query null
        if (!set.next()) {

            // set sku 1 apabila hasil query null
            sku = 1;

        } else {

            // increment 1 apabila terdapat hasil query
            sku = set.getInt(1);

            sku = sku + 1;
        }

        return sku;
    }
}
