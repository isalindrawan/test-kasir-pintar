package Functional_Package;

public class barangModel {

    private final String nama;
    private final int sku;
    private final int stok;
    private final byte[] image;

    public barangModel(int sku, String nama, int stok, byte[] image) {

        this.sku = sku;
        this.nama = nama;
        this.stok = stok;
        this.image = image;
    }

    public int sku() {

        return sku;
    }

    public String nama() {

        return nama;
    }

    public int stok() {

        return stok;
    }

    public byte[] image() {

        return image;
    }
}
