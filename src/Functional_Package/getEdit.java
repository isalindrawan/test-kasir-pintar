package Functional_Package;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

// class untuk mengambil nomor sku pada database
public class getEdit {

    private final Statement state;
    private final ResultSet set;
    private final Connection con;
    private final getConnection connect;
    private final barangModel barang;

    public getEdit(int sku) throws SQLException {

        // inisiasi kelas get connection
        connect = new getConnection();

        // koneksi ke database
        con = connect.connect();

        // inisiasi query
        state = con.createStatement();

        // eksekusi query
        set = state.executeQuery("SELECT * FROM barang WHERE `sku` = " + String.valueOf(sku));

        set.next();

        // impan hasil query ke variabel barang
        barang = new barangModel(set.getInt(1), set.getString(2), set.getInt(3), set.getBytes(4));
    }

    public barangModel getData() {

        return barang;
    }
}
