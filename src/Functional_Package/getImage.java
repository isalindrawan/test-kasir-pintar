package Functional_Package;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;

// class untuk mengambil gambar pada komputer
public class getImage {

    private BufferedImage image;
    private File file;
    private ImageIcon icon;
    private boolean state;

    public getImage() {

        // buka jendela file chooser dan buka file gambar
        JFileChooser choose = new JFileChooser();
        choose.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int returnValue = choose.showOpenDialog(null);

        if (returnValue == JFileChooser.APPROVE_OPTION) {

            // ambil file
            file = choose.getSelectedFile();

            try {

                // baca image pada file
                image = ImageIO.read(file);

            } catch (IOException ex) {
                Logger.getLogger(getImage.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {

            state = false;
        }
    }

    public BufferedImage getSourceImage() {

        // kembalian image
        return image;
    }

    public File getFile() {

        //kembalian file
        return file;
    }
}
