package Functional_Package;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

// class untuk melakukan operasi simpan data pada db
public class loggingData {

    private final PreparedStatement prep;
    private Statement state;
    private ResultSet set;
    private final Connection con;
    private final getConnection connect;

    public loggingData(int id, String jenis, int jumlah, int sku) throws FileNotFoundException, SQLException {

        // inisiasi class getConnection
        connect = new getConnection();

        // mulai koneksi
        con = connect.connect();

        // inisiasi insert query
        String Query = "INSERT INTO log (id, jenis, jumlah, tanggal, sku) VALUES (?, ?, ?, CURRENT_DATE(), ?)";

        // operasi insert query
        prep = con.prepareStatement(Query);

        // isi value denga parameter
        prep.setInt(1, id);
        prep.setString(2, jenis);
        prep.setInt(3, jumlah);
        prep.setInt(4, sku);

        prep.executeUpdate();
    }
}
