package Functional_Package;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class updateData {

    private final PreparedStatement prep;
    private Statement state;
    private ResultSet set;
    private final Connection con;
    private final getConnection connect;

    public updateData(int sku, String nama, int stok, byte[] image) throws FileNotFoundException, SQLException {

        // inisiasi class getConnection
        connect = new getConnection();

        // mulai koneksi
        con = connect.connect();

        // inisiasi insert query
        String Query = "UPDATE `barang` SET `sku`= ?,`nama`= ?,`stok`= ?,`gambar`= ? WHERE `sku` = ?";

        // operasi insert query
        prep = con.prepareStatement(Query);
        prep.setInt(1, sku);
        prep.setString(2, nama);
        prep.setInt(3, stok);
        prep.setBytes(4, image);
        prep.setInt(5, sku);

        prep.executeUpdate();
    }
}
