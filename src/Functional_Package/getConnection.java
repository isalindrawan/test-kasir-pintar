package Functional_Package;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

// class preferensi koneksi ke mysql database
public class getConnection {

    Connection con;

    public Connection connect() {

        //inisiasi / mulai koneksi ke mysql
        try {

            Class.forName("com.mysql.jdbc.Driver");

            // mulai koneksi
            con = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/kasirpintar", "root", "");

        } catch (ClassNotFoundException | SQLException e) {

            javax.swing.JOptionPane.showMessageDialog(null, e);
        }

        return con;
    }

    public boolean status() throws SQLException {

        // cek status koneksi
        return con.isClosed();
    }

    public void closeConnection() throws SQLException {

        // tutup koneksi
        con.close();
    }

}
