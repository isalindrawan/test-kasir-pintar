package Functional_Package;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

// konversi file ke data tipe array byte
public class fileToByte {

    private byte[] fileContent;

    public fileToByte(File file) throws IOException {

        fileContent = new byte[(int) file.length()];

        try (FileInputStream inputStream = new FileInputStream(file)) {

            inputStream.read(fileContent);

        } catch (IOException e) {

            throw new IOException("Unable to convert file to byte array. " + e.getMessage());

        }
    }

    public byte[] getByte() {

        return fileContent;
    }
}
