package Functional_Package;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

// class untuk melakukan operasi simpan data pada db
public class saveData {

    private final PreparedStatement prep;
    private Statement state;
    private ResultSet set;
    private final Connection con;
    private final getConnection connect;

    public saveData(int sku, String nama, int stok, File image) throws FileNotFoundException, SQLException {

        // inisiasi class getConnection
        connect = new getConnection();

        // mulai koneksi
        con = connect.connect();

        // inisiasi insert query
        String Query = "INSERT INTO barang(sku, nama, stok, gambar) VALUES (?, ?, ?, ?)";

        FileInputStream stream = new FileInputStream(image);

        // isi value dengan parameter
        prep = con.prepareStatement(Query);
        prep.setInt(1, sku);
        prep.setString(2, nama);
        prep.setInt(3, stok);

        prep.setBinaryStream(4, stream, (int) image.length());

        prep.executeUpdate();
    }
}
