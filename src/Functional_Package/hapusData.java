package Functional_Package;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

// class untuk melakukan operasi simpan data pada db
public class hapusData {

    private final PreparedStatement prep;
    private Statement state;
    private ResultSet set;
    private final Connection con;
    private final getConnection connect;

    public hapusData(int sku) throws FileNotFoundException, SQLException {

        // inisiasi class getConnection
        connect = new getConnection();

        // mulai koneksi
        con = connect.connect();

        // inisiasi insert query
        String Query = "DELETE FROM barang WHERE sku = ?";

        // operasi insert query
        prep = con.prepareStatement(Query);
        prep.setInt(1, sku);

        prep.executeUpdate();
    }
}
