package Functional_Package;

public class logModel {

    private final int id;
    private final int jumlah;
    private final String nama;
    private final String jenis;
    private final String tanggal;

    public logModel(int id, String nama, String jenis, int jumlah, String tanggal) {

        this.id = id;
        this.nama = nama;
        this.jenis = jenis;
        this.jumlah = jumlah;
        this.tanggal = tanggal;
    }

    public String getNama() {

        return nama;
    }

    public String getJenis() {

        return jenis;
    }

    public String getTanggal() {

        return tanggal;
    }

    public int getID() {

        return id;
    }

    public int getJumlah() {

        return jumlah;
    }

}
