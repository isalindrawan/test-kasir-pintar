package Functional_Package;

public class customTableModel extends javax.swing.table.AbstractTableModel {

    private final String[] columns;
    private final Object[][] rows;

    public customTableModel(Object[][] rows, String[] columns) {

        this.rows = rows;
        this.columns = columns;
    }

    @Override
    public Class getColumnClass(int column) {

        if (column == 3) {

            return javax.swing.Icon.class;
        }

        return getValueAt(0, column).getClass();
    }

    @Override
    public int getRowCount() {

        return this.rows.length;
    }

    @Override
    public int getColumnCount() {

        return this.columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        return this.rows[rowIndex][columnIndex];
    }

    @Override
    public String getColumnName(int col) {

        return this.columns[col];
    }

}
